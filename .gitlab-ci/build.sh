#!/bin/bash

set -e

export DEBIAN_FRONTEND=noninteractive
tag="${CI_COMMIT_TAG:-0.0.0+0}"
version="${tag%%+*}"
architecture="${ARCH:-all}"
tmpdir="$(mktemp -d)"

apt-get update
apt-get -qqy install \
    curl \
    dpkg-dev \
    g++ \
    libsystemd-dev \
    make

npm install -g pnpm

node --version
pnpm --version

if ! command -V nfpm >/dev/null 2>&1; then
    nfpm_version="${NFPM_VERSION:-2.41.1}"
    eval "$(dpkg-architecture -s)"
    curl -sfLo "${tmpdir}/nfpm.deb" "https://github.com/goreleaser/nfpm/releases/download/v${nfpm_version}/nfpm_${nfpm_version}_${DEB_HOST_ARCH}.deb"
    apt-get -qqy install "${tmpdir}/nfpm.deb"
fi

if [ ! -d "/tmp/${version}.tar.gz" ]; then
    curl -sfLo "/tmp/${version}.tar.gz" "https://github.com/Koenkk/zigbee2mqtt/archive/refs/tags/${version}.tar.gz"
fi
tar -C "${tmpdir}" -xzf "/tmp/${version}.tar.gz"
cd "${tmpdir}/zigbee2mqtt-${version}"
pnpm i --no-frozen-lockfile
pnpm run build
cd -

cat >"${tmpdir}/nfpm.yaml" <<EOF
name: zigbee2mqtt
arch: ${architecture}
version: ${tag}
version_schema: none
maintainer: "Stefan Heitmüller <stefan.heitmueller@gmx.com>"
description: Zigbee to MQTT bridge, get rid of your proprietary Zigbee bridges
homepage: https://www.zigbee2mqtt.io/
depends:
  - nodejs
contents:
  - src: ${tmpdir}/zigbee2mqtt-${version}/
    dst: /opt/zigbee2mqtt/
    type: tree
  - src: ${CI_PROJECT_DIR}/.packaging/zigbee2mqtt.service
    dst: /lib/systemd/system/
    file_info:
      mode: 0644
  - dst: /var/lib/zigbee2mqtt/data
    type: dir
    file_info:
      mode: 0700
      owner: zigbee2mqtt
      group: zigbee2mqtt
scripts:
  preinstall: ${CI_PROJECT_DIR}/.packaging/preinstall.sh
  postinstall: ${CI_PROJECT_DIR}/.packaging/postinstall.sh
EOF
nfpm package --config "${tmpdir}/nfpm.yaml" --packager deb
