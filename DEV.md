```bash
docker run --rm -it --tmpfs /tmp:exec -w /tmp --entrypoint bash -e CI_COMMIT_TAG=1.35.1+1 -e CI_PROJECT_DIR=/source -v $PWD:/source node:lts-slim
/source/.gitlab-ci/build.sh
```
