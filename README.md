# [zigbee2mqtt](https://zigbee2mqtt.io) Debian/Ubuntu Packages

Packages are built using [nfpm](https://nfpm.goreleaser.com/) by pushing released tags and repo is created using [Gitlabs static pages](https://morph027.gitlab.io/blog/repo-hosting-using-gitlab-pages/).

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-zigbee2mqtt.asc https://packaging.gitlab.io/zigbee2mqtt/gpg.key
```

### Add repo to apt

#### Old `list` Format

```
echo "deb https://packaging.gitlab.io/zigbee2mqtt zigbee2mqtt main" | sudo tee /etc/apt/sources.list.d/morph027-zigbee2mqtt.list
```

#### New `sources` Format

```bash
sudo tee /etc/apt/sources.list.d/morph027-zigbee2mqtt.sources <<EOF
Types: deb
URIs: https://packaging.gitlab.io/zigbee2mqtt
Suites: zigbee2mqtt
Components: main
Signed-By: /etc/apt/trusted.gpg.d/morph027-zigbee2mqtt.asc
EOF
```

### Install

```bash
sudo apt-get update
sudo apt-get install zigbee2mqtt morph027-keyring
```

### Configuration

See [Configuration](https://www.zigbee2mqtt.io/guide/installation/01_linux.html#configuring) and use `/var/lib/zigbee2mqtt/data/configuration.yaml`.

### Start

```bash
systemctl enable --now zigbee2mqtt
```

## Extras

### unattended-upgrades

To enable automatic upgrades using `unattended-upgrades`, just add the following config file:

```bash
echo 'Unattended-Upgrade::Allowed-Origins {"morph027:zigbee2mqtt";};' >/etc/apt/apt.conf.d/50zigbee2mqtt
```

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).
