getent passwd zigbee2mqtt >/dev/null 2>&1 || adduser \
  --system \
  --shell /bin/bash \
  --gecos 'Zigbee2MQTT' \
  --group \
  --disabled-password \
  --home /var/lib/zigbee2mqtt \
  zigbee2mqtt
