#!/bin/sh

set -e

use_systemctl="True"
debsystemctl=$(command -v deb-systemd-invoke || echo systemctl)
if ! command -V systemctl >/dev/null 2>&1; then
    use_systemctl="False"
fi

action="${1}"
if [ "${action}" = "configure" ] && [ -z "$2" ]; then
    action="install"
elif [ "${action}" = "configure" ] && [ -n "$2" ]; then
    action="upgrade"
fi

upgrade() {
    if [ "${use_systemctl}" = "True" ]; then
        state="$(systemctl is-active zigbee2mqtt || echo 'inactive')"
        if [ "${state}" = "active" ]; then
            systemctl --system daemon-reload >/dev/null || true
            $debsystemctl restart zigbee2mqtt
        fi
    fi
}

case "${action}" in
"1" | "install")
    :
    ;;
"2" | "upgrade")
    upgrade
    ;;
*)
    :
    ;;
esac
